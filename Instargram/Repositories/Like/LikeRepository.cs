﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Data;
using Instargram.Models;

namespace Instargram.Repositories.Like
{
    public class LikeRepository : Repository<Models.Like>, ILikeRepository

    {
        public LikeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public interface ILikeRepository : IRepository<Models.Like>
    {
    }
}
