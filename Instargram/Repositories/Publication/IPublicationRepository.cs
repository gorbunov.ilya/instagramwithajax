﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Repositories.Publication
{
    public interface IPublicationRepository : IRepository<Models.Publication>
    {
    }
}
