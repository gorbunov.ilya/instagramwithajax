﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Instargram.Repositories.Comment
{
    public interface ICommentRepository : IRepository<Models.Comment>
    {
    }
}
