﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Models;
using Instargram.Repositories.Comment;
using Instargram.Repositories.Like;
using Instargram.Repositories.Publication;
using Instargram.Repositories.Subscriber;

namespace Instargram.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ICommentRepository Comments { get; }
        IPublicationRepository Publications { get; }
        ISubscribersRepository Subscribers { get; }
        ILikeRepository Likes { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;

    }
}
