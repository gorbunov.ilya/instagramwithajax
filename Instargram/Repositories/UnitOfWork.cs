﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Data;
using Instargram.Models;
using Instargram.Repositories.Comment;
using Instargram.Repositories.Like;
using Instargram.Repositories.Publication;
using Instargram.Repositories.Subscriber;

namespace Instargram.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            Comments = new CommentRepository(context);
            Publications = new PublicationRepository(context);
            Subscribers = new SubscriberRepository(context);
            Likes = new LikeRepository(context);
        }
        public ISubscribersRepository Subscribers { get; }
        public ICommentRepository Comments { get; }
        public IPublicationRepository Publications { get; }
        public ILikeRepository Likes { get; }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
