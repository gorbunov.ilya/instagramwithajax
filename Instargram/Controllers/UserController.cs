﻿using System.Linq;
using System.Threading.Tasks;
using Instargram.Models;
using Instargram.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;

namespace Instargram.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UserController(UserManager<User> userManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index(string name)
        {
            var user = await _userManager.FindByNameAsync(name);
            if (user != null)
            {
                using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork()) 
                {
                    var subscription = unitOfWork.Subscribers.GetAllAsync()
                        .Result
                        .FirstOrDefault(s => s.UserFromId == _userManager.GetUserId(User) && s.UserToId == user.Id);
                    ViewBag.StateSubscription = subscription;
                    return View(user);
                }
            }

            return NotFound();
        }

        public IActionResult Search(string res, string key)
        {
            IQueryable<User> users;
            switch (key)
            {
                case "Поиск по логину":
                     users = _userManager.Users.Where(c => c.Email.Contains(res));
                    break;
                case "Почте":
                    users = _userManager.Users.Where(c => c.Email == res);
                    break;
                case "Имени":
                    users = _userManager.Users.Where(c => c.Name.Contains(res));
                    break;
                case "Информации о себе":
                    users = _userManager.Users.Where(c => c.AboutMe == res);
                    break;
                default:
                    users = _userManager.Users.Where(c => c.Email == res);
                    break;
            }

            return View(users);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Subscribe(string key)
        {
            var authorizeUser = await _userManager.FindByNameAsync(User.Identity.Name);
            var subscribeUser = await _userManager.FindByIdAsync(key);
            string subscriptionStatus = "Отписаться";
            using (var unitOfWork =_unitOfWorkFactory.MakeUnitOfWork())
            {
                var checkSubscriber = unitOfWork.Subscribers.GetAllAsync()
                    .Result
                    .FirstOrDefault(c => c.UserFromId == key && c.UserToId == authorizeUser.Id);
                
                if (checkSubscriber == null)
                {
                    Subscriber subscriber = new Subscriber() { UserFromId = key, UserToId = authorizeUser.Id, SubscriptionStatus = subscriptionStatus };
                    await unitOfWork.Subscribers.CreateAsync(subscriber);
                    authorizeUser.SubscriptionsCount++;
                    subscribeUser.SubscribersCount++;
                    
                }
                else
                {
                    subscriptionStatus = "Подписаться";
                    unitOfWork.Subscribers.RemoveAsync(checkSubscriber);
                    authorizeUser.SubscriptionsCount--;
                    subscribeUser.SubscribersCount--;
                }
                await unitOfWork.CompleteAsync();
                await _userManager.UpdateAsync(authorizeUser);
                await _userManager.UpdateAsync(subscribeUser);
                return PartialView("SubscribeAjaxResult",subscriptionStatus);
            }

        }
    }
}