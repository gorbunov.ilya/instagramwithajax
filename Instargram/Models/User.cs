﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Instargram.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public string AboutMe { get; set; }

        public string Gender { get; set; }
        public int PublicationCount { get; set; }
        public int SubscriptionsCount { get; set; }
        public int SubscribersCount { get; set; }

        public string ImagePath { get; set; }

        public List<Publication> Publications { get; set; }
        public List<Comment> Comments { get; set; }

        public List<Subscriber> Subscribers { get; set; }
        public List<Like> Likes { get; set; }
    }

}
