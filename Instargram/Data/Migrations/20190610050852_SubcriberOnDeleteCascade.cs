﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Instargram.Data.Migrations
{
    public partial class SubcriberOnDeleteCascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers",
                column: "UserFromId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribers_AspNetUsers_UserFromId",
                table: "Subscribers",
                column: "UserFromId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
