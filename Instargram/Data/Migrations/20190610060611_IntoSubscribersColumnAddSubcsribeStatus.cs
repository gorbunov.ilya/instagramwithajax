﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Instargram.Data.Migrations
{
    public partial class IntoSubscribersColumnAddSubcsribeStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SubscriptionStatus",
                table: "Subscribers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubscriptionStatus",
                table: "Subscribers");
        }
    }
}
