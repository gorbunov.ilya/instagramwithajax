﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Instargram.ViewModels
{
    public class PublicationViewModel
    {
        [Required]
        public IFormFile Image { get; set; }
        [Required]
        public string Description { get; set; }
    }
}