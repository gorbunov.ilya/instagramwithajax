﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Instargram.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Instargram.Helper
{
    public static class ImageHelper
    {
        public static async Task<string> SaveAvatarAndGetFullPath(string userId, IFormFile formFile, int flag, 
            IHostingEnvironment hostingEnvironment, Guid? guid)
        {
            string directoryPath = Path.Combine(hostingEnvironment.WebRootPath, "images", flag == 1 ? "avatars" : "publications");
            if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

            var filePath = Path.Combine(directoryPath, userId + guid + GetImageExtension(formFile.FileName));
            var relativeFilePath = Path.Combine("images", flag == 1 ? "avatars" : "publications", userId + guid + GetImageExtension(formFile.FileName));
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await formFile.CopyToAsync(fileStream);
                }

                return relativeFilePath;
        }

        public static string GetImageExtension(string fileName)
        {
            var parts = fileName.Split(".", StringSplitOptions.RemoveEmptyEntries);
            return "." + parts[parts.Length - 1];
        }
    }
}
